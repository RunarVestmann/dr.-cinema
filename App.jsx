import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Toast from 'react-native-toast-message';

import reducers from './src/reducers';
import AppContainer from './src/routes';

const ConnectedAppContainer = connect(null)(AppContainer);
const store = createStore(reducers, applyMiddleware(thunk));
export default () => (
  <Provider store={store}>
    <ConnectedAppContainer />
    <Toast ref={(ref) => Toast.setRef(ref)} />
  </Provider>
);
