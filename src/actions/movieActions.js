import * as movieService from '../services/movieService';
import * as constants from '../constants';
import { displayToastError } from '../services/toasterService';

const getMoviesByCinemaIdSuccess = (movies) => ({
  type: constants.GET_MOVIES_BY_CINEMA_ID,
  payload: movies,
});

const getUpcomingMoviesSuccess = (movies) => ({
  type: constants.GET_UPCOMING_MOVIES,
  payload: movies,
});

export const resetMovies = () => ({
  type: constants.RESET_MOVIES,
});

export const getUpcomingMovies = (successCallback) => async (dispatch) => {
  try {
    const movies = await movieService.getUpcomingMovies();
    successCallback(dispatch, getUpcomingMoviesSuccess(movies));
  } catch (error) {
    displayToastError(
      'Error occured',
      'Could not fetch upcoming movies, please try again',
    );
  }
};

export const getMoviesByCinemaId = (id, successCallback) => async (
  dispatch,
) => {
  try {
    const movies = await movieService.getMoviesByCinemaId(id);
    successCallback(dispatch, getMoviesByCinemaIdSuccess(movies));
  } catch (error) {
    displayToastError(
      'Error occured',
      'Could not fetch movies, please try again',
    );
  }
};
