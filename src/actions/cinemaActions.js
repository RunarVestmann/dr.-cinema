import * as cinemaService from '../services/cinemaService';
import * as constants from '../constants';
import { displayToastError } from '../services/toasterService';

const getAllCinemasSuccess = (allCinemas) => ({
  type: constants.GET_ALL_CINEMAS,
  payload: allCinemas,
});

export const getAllCinemas = (successCallback) => async (dispatch) => {
  try {
    const allCinemas = await cinemaService.getAllCinemas();
    successCallback(dispatch, getAllCinemasSuccess(allCinemas));
  } catch (error) {
    displayToastError(
      'Error occured',
      'Could not fetch all the cinemas, please try again',
    );
  }
};

export default getAllCinemas;
