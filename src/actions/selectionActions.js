import * as constants from '../constants';

export const selectCinema = (cinema) => ({
  type: constants.SELECT_CINEMA,
  payload: cinema,
});

export const selectMovie = (movie) => ({
  type: constants.SELECT_MOVIE,
  payload: movie,
});
