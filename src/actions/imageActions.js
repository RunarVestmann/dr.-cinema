import * as imageService from '../services/imagesService';
import * as constants from '../constants';

const getBackdropByMovieIdSuccess = (backDrops) => {
  return {
    type: constants.GET_BACKDROPS_BY_MOVIE_ID,
    payload: backDrops,
  };
};

// eslint-disable-next-line import/prefer-default-export
export const getBackdropByMovieId = (id, successCallback) => async (
  dispatch,
) => {
  try {
    const backDrops = await imageService.getBackDrops(id);
    successCallback(dispatch, getBackdropByMovieIdSuccess(backDrops));
  } catch (error) {}
};

export const resetBackdrops = () => ({
  type: constants.RESET_BACKDROPS,
});
