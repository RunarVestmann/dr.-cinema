import React from 'react';
import { FlatList, View } from 'react-native';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import Showtime from '../Showtime';

const ShowtimeModal = ({ isOpen, closeModal, title, showtimes }) => (
  <Modal isOpen={isOpen} closeModal={closeModal} title={title} height="60%">
    <View style={{ flex: 1, paddingVertical: 20 }}>
      <FlatList
        data={showtimes}
        renderItem={({ item }) => (
          <Showtime purchaseUrl={item.purchase_url} time={item.time} />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  </Modal>
);

ShowtimeModal.propTypes = {
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  title: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  showtimes: PropTypes.array,
};

ShowtimeModal.defaultProps = {
  isOpen: false,
  closeModal: () => {},
  title: 'Showtimes',
  showtimes: [],
};

export default ShowtimeModal;
