import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingTop: 10,
    justifyContent: 'center',
    borderWidth: 2,
    backgroundColor: 'rgba(255,255, 255, 0.9)',
    height: 150,
    width: '100%',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    padding: 10,
  },
  cinemaTitle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 22,
    paddingBottom: 5,
  },
  cinemaSub: {
    color: 'black',
    textAlign: 'center',
    fontSize: 15,
  },
  cinemaDescription: {
    color: 'black',
    textAlign: 'center',
    margin: 15,
    fontSize: 14,
  },

});
