import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CinemaDescriptionModal from '../CinemaDescriptionModal';
import styles from './styles';
import linkStyles from '../Link/styles';
import { buttonColor } from '../../styles/colors';

class CinemaDetail extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      isModalOpen: false,
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { cinema } = this.props;
    const { isModalOpen } = this.state;
    const more = `More about ${cinema.name}`;
    return (
      <View style={styles.container}>
        <CinemaDescriptionModal
          isOpen={isModalOpen}
          closeModal={() =>
            this._isMounted ? this.setState({ isModalOpen: false }) : {}
          }
          title={cinema.name}
          description={cinema.description ? cinema.description : ''}
        />
        <Text style={styles.cinemaTitle}>{cinema.name}</Text>
        <Text style={styles.cinemaSub}>
          {cinema.address}, {cinema.city}
        </Text>
        <Text style={styles.cinemaSub}>
          {cinema.phone}
          {cinema.phone && cinema.website && ' | '}
          {cinema.website}
        </Text>
        {cinema.description !== null && (
          <View>
            {cinema.description.length > 50 ? (
              <View style={{ paddingTop: 12 }}>
                <TouchableOpacity
                  onPress={() =>
                    this._isMounted ? this.setState({ isModalOpen: true }) : {}
                  }
                  style={[linkStyles.button, { backgroundColor: buttonColor }]}
                  activeOpacity={0.4}
                >
                  <Text style={linkStyles.text}>{more}</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <Text style={styles.cinemaDescription} numberOfLines={1}>
                {cinema.description}
              </Text>
            )}
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = ({ selectionReducer: { cinema } }) => ({
  cinema,
});

CinemaDetail.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  cinema: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(CinemaDetail);
