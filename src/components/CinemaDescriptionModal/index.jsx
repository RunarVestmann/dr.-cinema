import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import Modal from '../Modal';

const CinemaDescriptionModal = ({ isOpen, closeModal, title, description }) => {
  const cleanDescription = description.replace(
    /(<|&lt;)(br|b)\s*\/*(>|&gt;)/g,
    '',
  );

  return (
    <Modal isOpen={isOpen} closeModal={closeModal} title={title} heigth={'40%'}>
      <View style={{ padding: 20 }}>
        <Text style={{ textAlign: 'center' }}>{cleanDescription}</Text>
      </View>
    </Modal>
  );
};

CinemaDescriptionModal.propTypes = {
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  title: PropTypes.string,
  description: PropTypes.string,
};

CinemaDescriptionModal.defaultProps = {
  isOpen: false,
  closeModal: () => {},
  title: '',
  description: '',
};

export default CinemaDescriptionModal;
