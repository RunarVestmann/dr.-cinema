import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ratings: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: 4,
  },
  rating: {
    paddingHorizontal: 6,
    paddingVertical: 2,
    borderWidth: 1,
    borderRadius: 14,
    borderColor: '#ddd',
    marginRight: 4,
    marginBottom: 4,
  },
  ratingText: {
    fontSize: 10,
    opacity: 0.9,
  },
  box: {
    borderWidth: 1,
    width: 100,
    borderColor: '#ccc',
    height: 5,
  },
  rate: {
    borderWidth: 1,
    backgroundColor: 'red',
    width: 80,
    height: 4,
  },
});
