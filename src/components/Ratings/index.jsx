import * as React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const MAX_VALUE = 80;

const getValue = (src, val) => {
  let value = 0;
  switch (src) {
    case 'Internet Movie Database':
      value = parseFloat(val.replace('/10', '')) * 10;
      break;

    case 'Rotten Tomatoes':
      value = parseFloat(val.replace('%', ''));
      break;

    case 'Metacritic':
      value = parseFloat(val.replace('/100', ''));
      break;
    default:
      value = 50;
  }
  return (value / 100) * MAX_VALUE;
};
const getName = (src) => {
  let value = 0;
  switch (src) {
    case 'Internet Movie Database':
      value = 'IMDB';
      break;
    default:
      value = src;
  }
  return value;
};

const Ratings = ({ ratings }) => (
  <View style={styles.ratings}>
    {ratings.map((r, index) => (
      <View style={styles.rating} key={index.toString()}>
        <Text style={styles.ratingText}>{getName(r.Source)}</Text>
        <View style={[styles.box, { width: MAX_VALUE }]}>
          <View style={[styles.rate, { width: getValue(r.Source, r.Value) }]} />
        </View>
        <Text style={styles.ratingText}>{r.Value}</Text>
      </View>
    ))}
  </View>
);

Ratings.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  ratings: PropTypes.array,
};

Ratings.defaultProps = {
  ratings: [],
};

export default Ratings;
