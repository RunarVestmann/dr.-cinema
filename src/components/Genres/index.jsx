import * as React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const Genres = ({ genres, size }) => {
  const list = genres.split(',');
  return (
    <View style={styles.genres}>
      {list.map((genre) => (
        <View key={genre} style={styles.genre}>
          <Text style={[styles.genreText, { fontSize: size }]}>{genre}</Text>
        </View>
      ))}
    </View>
  );
};

export default Genres;
