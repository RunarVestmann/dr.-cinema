import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  genres: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: 4,
  },
  genre: {
    paddingHorizontal: 6,
    paddingVertical: 2,
    borderWidth: 1,
    borderRadius: 14,
    borderColor: '#ddd',
    marginRight: 4,
    marginBottom: 4,
  },
  genreText: {
    fontSize: 10,
    opacity: 0.9,
  },
});
