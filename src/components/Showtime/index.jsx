/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import Link from '../Link';

const Showtime = ({ purchaseUrl, time }) => (
  <View style={{ padding: 10, paddingHorizontal: 60 }}>
    <Text style={{ textAlign: 'left', fontWeight: 'bold' }}>
      {new Date().toDateString()}
    </Text>
    <Text style={{ textAlign: 'left', marginBottom: 5 }}>{time}</Text>
    <Link url={purchaseUrl} textSize={12}>
      Purchase Tickets
    </Link>
  </View>
);

Showtime.propTypes = {
  purchaseUrl: PropTypes.string,
  time: PropTypes.string,
};

Showtime.defaultProps = {
  purchaseUrl: '',
  time: '',
};

export default Showtime;
