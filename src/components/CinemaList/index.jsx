import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';

import CinemaThumbnail from '../CinemaThumbnail';

const CinemaList = ({ cinemas }) => (
  <FlatList
    data={cinemas}
    extraData={cinemas}
    numColumns={1}
    renderItem={({ item }) => <CinemaThumbnail cinema={item} />}
    keyExtractor={({ id }) => id.toString()}
  />
);

CinemaList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  cinemas: PropTypes.array,
};

CinemaList.defaultProps = {
  cinemas: [],
};

export default CinemaList;
