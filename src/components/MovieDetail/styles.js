import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    height: '100%',
  },

  title: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    textAlign: 'center',
  },
  text: {
    fontSize: 15,
    textAlign: 'center',
    paddingBottom: 5,
    color: 'rgba(0,0,0,0.7)',
  },
  textContainer: {
    alignContent: 'flex-end',
    paddingHorizontal: 20,
    backgroundColor: 'rgba(255,255,255, 0.8)',
    borderRadius: 34,
    width: '100%',
  },
  button: {
    backgroundColor: '#2196F3',
    color: 'white',
    marginVertical: 10,
    paddingVertical: 10,
    marginTop: 5,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: 'rgba(0,0,0,0.1)',
    width: '100%',
    fontSize: 12,
    textAlign: 'center',
  },
  comming: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
