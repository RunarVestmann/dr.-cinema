import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './styles';
import Genres from '../Genres';
import Ratings from '../Ratings';
import { buttonColor } from '../../styles/colors';
import ShowtimeModal from '../ShowtimeModal';
import { getCurrentShowtimes } from '../../services/movieService';

class MovieDetail extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
    this.state = {
      isShowtimeModalOpen: false,
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const {
      movie: { movie },
      cinema,
    } = this.props;
    const { isShowtimeModalOpen } = this.state;

    const currShowtime = getCurrentShowtimes(movie.Showtimes, cinema);
    return (
      <View style={{ width: '100%' }}>
        <ShowtimeModal
          isOpen={isShowtimeModalOpen}
          closeModal={() =>
            this._isMounted ? this.setState({ isShowtimeModalOpen: false }) : {}
          }
          title={`Showtimes at ${cinema.name}`}
          showtimes={currShowtime}
        />

        <View style={styles.textContainer}>
          <View>
            <View>
              <Text style={styles.title}>{movie.Title}</Text>
              <Text style={styles.text}>
                {movie.Runtime}
                {'  '}|{'  '}
                {movie.Released}
              </Text>
              {movie.Genre && <Genres genres={movie.Genre} size={14} />}
              {movie.Ratings && <Ratings ratings={movie.Ratings} />}
            </View>
            {movie.ReleaseDateIS !== undefined && (
              <Text style={styles.comming}>
                Coming: {new Date(movie.ReleaseDateIS).toDateString()}
              </Text>
            )}
          </View>

          <Text style={styles.text}>{movie.Plot}</Text>
          {movie.ReleaseDateIS === undefined && currShowtime != null && (
            <TouchableOpacity
              onPress={() =>
                this._isMounted
                  ? this.setState({ isShowtimeModalOpen: true })
                  : {}
              }
            >
              <Text style={[styles.button, { backgroundColor: buttonColor }]}>
                Get Showtimes
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ selectionReducer: { movie, cinema } }) => ({
  movie,
  cinema,
});

MovieDetail.propTypes = {
  movie: PropTypes.object,
  cinema: PropTypes.object,
};

MovieDetail.defaultProps = {
  movie: {},
  cinema: {},
};

// eslint-disable-next-line max-len
export default connect(mapStateToProps)(MovieDetail);
