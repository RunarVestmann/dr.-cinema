import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  img: {
    width: '100%',
    resizeMode: 'cover',
    borderRadius: 24,
    margin: 0,
    marginBottom: 10,
  },
  clapperImg: {
    height: 160,
    width: 130,
    marginBottom: 30,
    tintColor: 'rgba(90,90,90,0.9)',
  },
  clapperContainer: {
    flex: 1,
    marginHorizontal: 60,
    marginTop: -20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 34,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'rgba(90,90,90,0.9)',
  },
  titleContainer: {
    backgroundColor: 'rgba(0,0,0, 0.04)',
    borderRadius: 15,
    paddingVertical: 5,
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 14,
  },
});
