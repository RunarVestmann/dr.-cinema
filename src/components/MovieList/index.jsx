/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { Animated, Dimensions, Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getMoviesByCinemaId, resetMovies } from '../../actions/movieActions';
import MovieListItem from '../MovieListItem';
import Spinner from '../Spinner';
import clapperImg from '../../resources/clapperboard.png';
import styles from './styles';

const { width } = Dimensions.get('window');
const ITEM_SIZE = width * 0.7;

class MovieList extends React.Component {
  constructor(props) {
    super(props);

    this._isMounted = true;

    this.state = {
      scrollX: new Animated.Value(0),
      loading: true,
    };
  }

  componentDidMount() {
    const {
      getMoviesByCinemaId,
      resetMovies,
      cinema,
      getMoviesFunction,
    } = this.props;
    resetMovies();

    if (getMoviesFunction !== undefined) {
      getMoviesFunction((dispatch, movieAction) => {
        if (this._isMounted) {
          dispatch(movieAction);
          this.setState({ loading: false });
        }
      });
    } else {
      getMoviesByCinemaId(cinema.id, (dispatch, movieAction) => {
        if (this._isMounted) {
          dispatch(movieAction);
          this.setState({ loading: false });
        }
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { movies } = this.props;

    const { scrollX, loading } = this.state;
    const displaymovies = [{ key: 'left' }, ...movies, { key: 'right' }];
    // eslint-disable-next-line no-nested-ternary
    return loading ? (
      <Spinner />
    ) : movies.length === 0 ? (
      <View style={styles.clapperContainer}>
        <Image source={clapperImg} style={styles.clapperImg} />
        <Text style={styles.text}>No movie showing today</Text>
      </View>
    ) : (
      <Animated.FlatList
        showsHorizontalScrollIndicator={false}
        data={displaymovies}
        keyExtractor={({ Id, key }) => key || Id.toString()}
        horizontal
        contentContainerStyle={{ alignItems: 'center' }}
        snapToInterval={ITEM_SIZE}
        decelerationRate={0}
        bounces={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
        scrollEventThrottle={16}
        renderItem={({ item, index }) => (
          <MovieListItem item={item} index={index} scrollX={scrollX} />
        )}
      />
    );
  }
}
const mapStateToProps = ({ movieReducer, selectionReducer: { cinema } }) => ({
  movies: movieReducer,
  cinema,
});

MovieList.propTypes = {
  movies: PropTypes.array,
  cinema: PropTypes.object,
  getMoviesFunction: PropTypes.func,
  getMoviesByCinemaId: PropTypes.func.isRequired,
  resetMovies: PropTypes.func.isRequired,
};

MovieList.defaultProps = {
  movies: [],
  getMoviesFunction: undefined,
  cinema: {},
};

export default connect(mapStateToProps, { getMoviesByCinemaId, resetMovies })(
  MovieList,
);
