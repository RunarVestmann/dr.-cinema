import React from 'react';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { View, Image, Text } from 'react-native';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Link from '../Link';
import { selectCinema } from '../../actions/selectionActions';
import logoArray from '../../resources/logos';
import styles from './styles';

const CinemaThumbnail = ({
  cinema,
  // eslint-disable-next-line react/prop-types
  navigation: { navigate },
  // eslint-disable-next-line no-shadow
  selectCinema,
}) => {
  const logos = logoArray();
  let currentLogo;
  try {
    currentLogo = logos[cinema.id - 1];
  } catch (err) {}
  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          selectCinema(cinema);
          navigate('CinemaDetails');
        }}
      >
        <View style={styles.spacer}>
          {currentLogo !== undefined ? (
            <Image source={currentLogo} style={styles.logo} />
          ) : (
            <Text style={styles.cinemaText}>{cinema.name}</Text>
          )}
        </View>
      </TouchableOpacity>
      <View style={styles.link}>
        <Link url={cinema.website} textSize={14}>
          {cinema.website}
        </Link>
      </View>
    </View>
  );
};

CinemaThumbnail.propTypes = {
  cinema: PropTypes.object,
  selectCinema: PropTypes.func.isRequired,
};

CinemaThumbnail.defaultProps = {
  item: {},
};

export default connect(null, { selectCinema })(withNavigation(CinemaThumbnail));
