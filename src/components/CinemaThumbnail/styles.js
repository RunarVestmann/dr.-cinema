import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  cinemaText: {
    fontSize: 28, fontWeight: 'bold',
  },
  spacer: {
    width: '100%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(220,220,220,0.6)',
  },
  logo: {
    resizeMode: 'contain',
    width: 180,
  },
  link: {
    marginTop: 0,
    borderTopWidth: 1,
    borderBottomWidth: 2,
  },
});
