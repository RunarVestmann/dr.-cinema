import React from 'react';
import { Image, View, Animated, Dimensions, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import styles from './styles';
import Genres from '../Genres';
import { selectMovie } from '../../actions/selectionActions';
import {
  getBackdropByMovieId,
  resetBackdrops,
} from '../../actions/imageActions';
import { render } from 'react-dom';

const { width } = Dimensions.get('window');
const SPACING = 10;
const ITEM_SIZE = width * 0.7;
const OFFSET_SPACER = (width - ITEM_SIZE) / 2;

class MovieListItem extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const {
      item,
      index,
      scrollX,
      selectMovie,
      getBackdropByMovieId,
      resetBackdrops,
      navigation: { navigate },
    } = this.props;
    if (!item.Poster) {
      return <View style={{ width: OFFSET_SPACER }} />;
    }
    const inputRange = [
      (index - 2) * ITEM_SIZE,
      (index - 1) * ITEM_SIZE,
      index * ITEM_SIZE,
    ];
    const translateY = scrollX.interpolate({
      inputRange,
      outputRange: [0, -30, 0],
    });
    return (
      <View style={{ width: ITEM_SIZE }}>
        <Animated.View
          style={[
            styles.list,
            [
              {
                marginHorizontal: SPACING,
                padding: SPACING * 2,
                transform: [{ translateY }],
              },
            ],
          ]}
        >
          <TouchableOpacity
            onPress={() => {
              selectMovie({ movie: { ...item } });
              if (this._isMounted) {
                resetBackdrops();
              }
              if (item.ImdbID !== null) {
                getBackdropByMovieId(item.ImdbID, (dispatch, imageAction) => {
                  if (this._isMounted) {
                    dispatch(imageAction);
                  }
                });
              }
              navigate('MovieDetails');
            }}
            activeOpacity={0.7}
            style={{ width: 200 }}
          >
            <Image
              source={{ uri: item.Poster }}
              style={[
                styles.img,
                {
                  height: ITEM_SIZE * 1.1,
                },
              ]}
            />

            <View style={styles.titleContainer}>
              <Text style={styles.title} numberOfLines={1}>
                {item.Title}
              </Text>
              <Text style={styles.subTitle}>{item.Released}</Text>
              {item.ReleaseDateIS !== undefined && (
                <Text style={styles.comming}>
                  Coming: {new Date(item.ReleaseDateIS).toDateString()}
                </Text>
              )}
              {item.Genre && <Genres genres={item.Genre} />}
            </View>
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }
}

MovieListItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  item: PropTypes.object.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  scrollX: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  selectMovie: PropTypes.func.isRequired,
  getBackdropByMovieId: PropTypes.func.isRequired,
  resetBackdrops: PropTypes.func.isRequired,
};

// eslint-disable-next-line max-len
export default connect(null, {
  selectMovie,
  getBackdropByMovieId,
  resetBackdrops,
})(withNavigation(MovieListItem));
