import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  list: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 34,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0, 0.5)',
  },
  img: {
    width: '100%',
    resizeMode: 'cover',
    borderRadius: 24,
    margin: 0,
    marginBottom: 10,
  },
  titleContainer: {
    backgroundColor: 'rgba(0,0,0, 0.04)',
    borderRadius: 15,
    paddingVertical: 5,
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 14,
  },
  comming: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
