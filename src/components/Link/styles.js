import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: {
    height: 36,
    width: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
    color: 'white',
    textTransform: 'uppercase',
  },
});
