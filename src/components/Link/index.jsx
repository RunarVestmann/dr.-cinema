import React, { useCallback } from 'react';
import { Alert, Linking, View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { buttonColor } from '../../styles/colors';
import styles from './styles';

// eslint-disable-next-line react/prop-types
const Link = ({ url, children, textSize }) => {
  const handlePress = useCallback(async () => {
    const fixedUrl =
      url[0] === 'w' || !url.includes('http') ? 'https://' + url : url;
    try {
      await Linking.openURL(fixedUrl);
    } catch (error) {
      Alert.alert(`Don't know how to open this URL: ${fixedUrl}`);
    }
  }, [url]);

  return (
    <View>
      <TouchableOpacity
        style={[styles.button, { backgroundColor: buttonColor }]}
        onPress={handlePress}
        activeOpacity={0.4}
      >
        <Text style={[styles.text, { fontSize: textSize }]}>{children}</Text>
      </TouchableOpacity>
    </View>
  );
};

Link.propTypes = {
  url: PropTypes.string.isRequired,
};

export default Link;
