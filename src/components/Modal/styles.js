import { StyleSheet, Dimensions } from 'react-native';

const { width: winWidth } = Dimensions.get('window');

export default StyleSheet.create({
  modal: {
    alignItems: 'center',
  },
  body: {
    alignItems: 'center',
    borderRadius: 10,
    width: winWidth - 70,
    backgroundColor: 'rgba(200,200,200,.92)',
  },
  title: {
    paddingTop: 10,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
