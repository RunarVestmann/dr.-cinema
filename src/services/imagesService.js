/* eslint-disable no-return-await */
import { getToken } from './tokenService';
import { BASE_API_URL } from '../constants';

const IMAGES_API_URL = `${BASE_API_URL}images/?imdbid=`;
const IMAGES_IMDB_URL = 'https://image.tmdb.org/t/p/w500';

const getImagesFromEndpoint = async (endpoint) => {
  const response = await fetch(endpoint, {
    method: 'GET',
    headers: {
      'x-access-token': await getToken(),
    },
  });

  const posters = await response.json();
  return posters;
};

// eslint-disable-next-line import/prefer-default-export
export const getBackDrops = async (id) => {
  const imgs = await getImagesFromEndpoint(IMAGES_API_URL + id);
  // eslint-disable-next-line camelcase
  const imdbimgs = imgs[0].results.backdrops.map(
    ({ file_path }) => IMAGES_IMDB_URL + file_path,
  );
  return imdbimgs;
};
