import Toast from 'react-native-toast-message';

const position = 'top';
const visibilityTime = 2500;
const topOffset = 250;

export const displayToastSuccess = (text1, text2) => {
  Toast.show({
    type: 'success',
    position,
    text1,
    text2,
    visibilityTime,
    topOffset,
  });
};

export const displayToastError = (text1, text2) => {
  Toast.show({
    type: 'error',
    position,
    text1,
    text2,
    visibilityTime,
    topOffset,
  });
};
