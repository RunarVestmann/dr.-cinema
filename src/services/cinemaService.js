import { getToken } from './tokenService';
import { BASE_API_URL } from '../constants';
import { sortByName } from './sortService';

const API_URL = `${BASE_API_URL}theaters`;

export const getAllCinemas = async () => {
  const token = await getToken();
  const response = await fetch(API_URL, {
    method: 'GET',
    headers: {
      'x-access-token': token,
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json',
    },
  });

  const cinemas = await response.json();
  sortByName(cinemas);

  return cinemas.map((cinema) => {
    const address = cinema['address\t'];
    cinema.address = address;
    if (cinema.website.includes('sambio')) {
      cinema.website = 'www.sambio.is';
    } else if (cinema.website.includes('borgarbio')) {
      cinema.website = 'smarabio.is/borgarbio';
    }
    delete cinema['address\t'];
    return cinema;
  });
};

export default getAllCinemas;
