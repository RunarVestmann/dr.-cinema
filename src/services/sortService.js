const alphabet = 'aábcdðeéfghiíjklmnoópqrstuúvwxyzþæö';

export const sortByName = (array) => {
  array.sort((a, b) => {
    const name1 = a.name.toLowerCase().replace(',', '');
    const name2 = b.name.toLowerCase().replace(',', '');
    // Find the first position were the strings do not match
    let position = 0;
    while (name1[position] === name2[position]) {
      // If both are the same don't swap
      if (!name1[position] && !name2[position]) return 0;
      // Otherwise the shorter one goes first
      if (!name1[position]) return 1;
      if (!name2[position]) return -1;
      position += 1;
    }
    // Then sort by the characters position
    return (
      alphabet.indexOf(name1[position]) - alphabet.indexOf(name2[position])
    );
  });
};

export const sortByTitle = (array) => {
  array.sort((a, b) => {
    const name1 = a.Title.toLowerCase().replace(',', '');
    const name2 = b.Title.toLowerCase().replace(',', '');
    // Find the first position were the strings do not match
    let position = 0;
    while (name1[position] === name2[position]) {
      // If both are the same don't swap
      if (!name1[position] && !name2[position]) return 0;
      // Otherwise the shorter one goes first
      if (!name1[position]) return 1;
      if (!name2[position]) return -1;
      position += 1;
    }
    // Then sort by the characters position
    return (
      alphabet.indexOf(name1[position]) - alphabet.indexOf(name2[position])
    );
  });
};

export const sortByReleaseDateDescending = (array) => {
  array.sort((a, b) => {
    const aDate = new Date(a.ReleaseDateIS);
    const bDate = new Date(b.ReleaseDateIS);
    return bDate - aDate;
  });
};
