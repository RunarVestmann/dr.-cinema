/* eslint-disable no-return-await */
import { getToken } from './tokenService';
import cache from '../resources/cache';
import { BASE_API_URL } from '../constants';

import { sortByTitle, sortByReleaseDateDescending } from './sortService';

let allMoviesFetchTimestamp;
let upcomingMoviesFetchTimestamp;

const ALL_MOVIES_API_URL = `${BASE_API_URL}movies`;
const UPCOMING_MOVIES_API_URL = `${BASE_API_URL}upcoming`;
const GENRES_API_URL = `${BASE_API_URL}genres`;

const getMoviesFromEndpoint = async (endpoint) => {
  const response = await fetch(endpoint, {
    method: 'GET',
    headers: {
      'x-access-token': await getToken(),
      'Cache-Control': 'no-cache',
      Pragma: 'no-cache',
      Expires: 0,
    },
  });

  const movies = await response.json();
  return movies;
};

const mapper = (value, prop, alternativeValue) => {
  if (value) {
    if (value[prop]) return value[prop];
  }
  return alternativeValue;
};

const getGenresList = async () => {
  const response = await fetch(GENRES_API_URL, {
    method: 'GET',
    headers: {
      'x-access-token': await getToken(),
    },
  });
  const genresJson = await response.json();

  return genresJson;
};

const filterGenres = (genreArray, genreList) => {
  const genres = [];
  if (isNaN(genreArray[0])) {
    genreArray.forEach((obj) => {
      genres.push(obj['NameEN\t']);
    });
  } else {
    genreArray.forEach((id) => {
      const genreObj = genreList.filter((g) => g.ID == id)[0];
      genres.push(genreObj['NameEN\t']);
    });
  }
  return genres.join();
};

const transformResult = async (results) => {
  const genreList = await getGenresList();
  const movies = results.map((movie) => {
    const {
      id,
      title,
      poster,
      durationMinutes,
      plot,
      year,
      showtimes,
      trailers,
      omdb,
      genres,
    } = movie;
    return {
      Id: String(id),
      Title: title, // omdb[0].Title;
      Poster: poster,
      Runtime:
        durationMinutes !== undefined && durationMinutes !== 0
          ? `${durationMinutes.toString()} min`
          : 'N/A',
      Plot: mapper(omdb[0], 'Plot', plot), // omdb[0].Plot;
      Genre: filterGenres(genres, genreList), // omdb[0].Genre;
      Released: year, // omdb[0].Released;
      ImdbID: mapper(omdb[0], 'imdbID', null), // omdb[0].Released;
      Showtimes: showtimes,
      Trailer: getTrailerVideoId(trailers),
      ReleaseDateIS: movie['release-dateIS'],
      Ratings: mapper(omdb[0], 'Ratings', null),
    };
    // genres: genre_ids.map((genre) => genres[genre]),
  });
  return movies;
};

const cacheStillValid = (timestamp) =>
  timestamp !== undefined && timestamp + 7200 >= new Date() / 1000;

export const getAllMovies = async () => {
  if (cacheStillValid(allMoviesFetchTimestamp)) {
    return Object.values(cache.allMovies);
  }

  const movies = await getMoviesFromEndpoint(ALL_MOVIES_API_URL);

  allMoviesFetchTimestamp = new Date() / 1000;

  movies.forEach((movie) => {
    cache.allMovies[movie.id] = movie;
  });

  return movies;
};

export const getUpcomingMovies = async () => {
  if (cacheStillValid(upcomingMoviesFetchTimestamp)) {
    const returnValue = Object.values(cache.upcomingMovies);
    sortByReleaseDateDescending(returnValue);
    return returnValue;
  }
  const movies = await getMoviesFromEndpoint(UPCOMING_MOVIES_API_URL);
  upcomingMoviesFetchTimestamp = new Date() / 1000;

  const transformedMovies = await transformResult(movies);
  transformedMovies.sort((a, b) => {
    const aDate = new Date(a.ReleaseDateIS);
    const bDate = new Date(b.ReleaseDateIS);
    return bDate - aDate;
  });

  transformedMovies.forEach((movie) => {
    if (movie.id !== undefined) {
      cache.upcomingMovies[movie.id] = movie;
    } else {
      cache.upcomingMovies[movie.Id] = movie;
    }
  });

  return transformedMovies;
};

export const getMoviesByCinemaId = async (id) => {
  const movies = await getAllMovies();

  const m = movies.filter((mov) => {
    return mov.showtimes.some(
      (s) => s.cinema.id === id || Number(s.cinema) === id,
    );
  });
  const transformedMovies = await transformResult(m);

  sortByTitle(transformedMovies);

  return transformedMovies;
};

const getTrailerVideoId = (trailers) => {
  if (
    trailers !== undefined &&
    trailers.length > 0 &&
    trailers[0].results !== undefined &&
    trailers[0].results.length > 0
  ) {
    const trailerUrlSplit = trailers[0].results[0].url.split('/');
    return trailerUrlSplit[trailerUrlSplit.length - 1];
  }
  return undefined;
};

export const getCurrentShowtimes = (showtimes, cinema) => {
  const showtimeFilter =
    showtimes !== undefined && showtimes.length > 0
      ? showtimes.filter((s) =>
          s.cinema.id !== undefined
            ? s.cinema.id == cinema.id
            : s.cinema == cinema.id,
        )
      : null;
  return showtimeFilter !== null ? showtimeFilter[0].schedule : null;
};
