/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { ImageBackground } from 'react-native';

import MovieList from '../../components/MovieList';
import CinemaDetail from '../../components/CinemaDetail';
import bgImage from '../../resources/background.jpg';

const CinemaDetails = () => (
  <ImageBackground style={{ flex: 1 }} source={bgImage}>
    <MovieList />
    <CinemaDetail />
  </ImageBackground>
);

export default CinemaDetails;
