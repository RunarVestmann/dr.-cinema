import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ImageBackground } from 'react-native';
import { getUpcomingMovies } from '../../actions/movieActions';
import MovieList from '../../components/MovieList';
import bgImage from '../../resources/background.jpg';

// eslint-disable-next-line no-shadow
const UpcomingMovies = ({ getUpcomingMovies }) => (
  <ImageBackground style={{ flex: 1 }} source={bgImage}>
    <MovieList getMoviesFunction={getUpcomingMovies} />
  </ImageBackground>
);

UpcomingMovies.propTypes = {
  getUpcomingMovies: PropTypes.func.isRequired,
};

export default connect(null, { getUpcomingMovies })(UpcomingMovies);
