import React from 'react';
import {
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  View,
} from 'react-native';
import styles from './styles';
import bgImage from '../../resources/background.jpg';
import logo from '../../resources/logo.png';

const Main = (props) => {
  const {
    // eslint-disable-next-line react/prop-types
    navigation: { navigate },
  } = props;
  return (
    <ImageBackground source={bgImage} style={styles.container}>
      <Image source={logo} style={styles.logo} />
      <Text style={styles.title}>Dr. Cinema</Text>
      <View>
        <TouchableOpacity
          onPress={() => {
            navigate('Cinemas');
          }}
          style={styles.button}
        >
          <Text style={styles.buttonText}>All Movie Theaters</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigate('UpcomingMovies');
          }}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Upcoming Movies</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default Main;
