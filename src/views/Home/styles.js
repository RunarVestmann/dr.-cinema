import { StyleSheet } from 'react-native';
import { buttonColor } from '../../styles/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  title: {
    fontSize: 56,
    fontWeight: 'bold',
    color: 'rgba(0,0,0,0.7)',
  },
  button: {
    justifyContent: 'center',
    height: 70,
    width: 250,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderColor: 'rgba(0,0,0,0.1)',
    borderWidth: 2,
    backgroundColor: buttonColor,
    borderRadius: 20,
    marginTop: 20,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
  },
  logo: {
    height: 210,
    width: 350,
    resizeMode: 'stretch',
    marginBottom: -50,
    marginTop: -70,
    opacity: 0.8,
  },
});
