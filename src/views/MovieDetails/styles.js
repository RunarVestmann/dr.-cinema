import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    height: '100%',
  },
  ScrollViewer: {
    paddingTop: 150,
    alignItems: 'center',
  },
  poster: {
    height: 200,
    position: 'absolute',
    top: 1,
  },
  gradient: {
    height: 100,
    position: 'absolute',
    top: 101,
  },
});
