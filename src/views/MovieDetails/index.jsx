/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { View, Image, ImageBackground, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient';
import YoutubePlayer from 'react-native-youtube-iframe';
import { ScrollView } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';

import MovieDetail from '../../components/MovieDetail';
import styles from './styles';
import bgImage from '../../resources/background.jpg';

const MovieDetails = (props) => {
  const { height, width } = Dimensions.get('window');
  const {
    movie: { movie },
    imageReducer,
  } = props;
  const { Trailer } = movie;
  let { Poster } = movie;
  if (imageReducer !== '') {
    Poster = imageReducer;
  }

  return (
    <ImageBackground style={styles.container} source={bgImage}>
      <View style={{ width: '100%' }}>
        <ScrollView
          contentContainerStyle={[
            styles.ScrollViewer,
            { height: height * 0.98 },
          ]}
        >
          <Image source={{ uri: Poster }} style={[styles.poster, { width }]} />
          <LinearGradient
            colors={['rgba(0, 0, 0, 0)', 'white']}
            style={[styles.gradient, { width }]}
          />
          <View style={{ height: 50 }} />
          <MovieDetail />
          {Trailer !== undefined && (
            <View style={{ marginVertical: 10 }}>
              <YoutubePlayer width={300} height={500} videoId={Trailer} />
            </View>
          )}
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = ({ selectionReducer: { movie }, imageReducer }) => ({
  movie,
  imageReducer,
});

MovieDetails.propTypes = {
  movie: PropTypes.object,
  imageReducer: PropTypes.string,
};

MovieDetails.defaultProps = {
  movie: {},
  imageReducer: '',
};

export default connect(mapStateToProps)(MovieDetails);
