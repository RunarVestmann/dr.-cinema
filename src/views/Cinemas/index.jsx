import React from 'react';
import { ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getAllCinemas } from '../../actions/cinemaActions';
import CinemaList from '../../components/CinemaList';
import Spinner from '../../components/Spinner';
import bgImage from '../../resources/background.jpg';

class Cinemas extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = true;

    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    const { getAllCinemas } = this.props;
    getAllCinemas((dispatch, cinemaAction) => {
      if (this._isMounted) {
        dispatch(cinemaAction);
        this.setState({ loading: false });
      }
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { cinemas } = this.props;
    const { loading } = this.state;
    return loading ? (
      <Spinner />
    ) : (
      <ImageBackground
        source={bgImage}
        style={{
          width: '100%',
        }}
      >
        <CinemaList cinemas={cinemas} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = ({ cinemaReducer }) => ({ cinemas: cinemaReducer });

Cinemas.propTypes = {
  getAllCinemas: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  cinemas: PropTypes.array,
};

Cinemas.defaultProps = {
  cinemas: [],
};

export default connect(mapStateToProps, { getAllCinemas })(Cinemas);
