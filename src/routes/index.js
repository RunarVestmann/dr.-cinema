import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Cinemas from '../views/Cinemas';
import CinemaDetails from '../views/CinemaDetails';
import Home from '../views/Home';
import UpcomingMovies from '../views/UpcomingMovies';
import MovieDetails from '../views/MovieDetails';

export default createAppContainer(
  createStackNavigator({
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown: false,
      },
    },
    UpcomingMovies: {
      screen: UpcomingMovies,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Upcoming Movies',
        headerTitleAlign: 'center',
      },
    },
    Cinemas: {
      screen: Cinemas,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Cinemas',
        headerTitleAlign: 'center',
      },
    },
    CinemaDetails: {
      screen: CinemaDetails,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Cinema Details',
        headerTitleAlign: 'center',
      },
    },
    MovieDetails: {
      screen: MovieDetails,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Movie Details',
        headerTitleAlign: 'center',
      },
    },
  }),
);
