const allCinemas = {};
const allMovies = {};
const upcomingMovies = {};

export default {
  allCinemas,
  allMovies,
  upcomingMovies,
};
