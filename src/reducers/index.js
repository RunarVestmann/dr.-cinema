import { combineReducers } from 'redux';

import cinemaReducer from './cinemaReducer';
import movieReducer from './movieReducer';
import selectionReducer from './selectionReducer';
import imageReducer from './imageReducer';

export default combineReducers({
  cinemaReducer,
  movieReducer,
  imageReducer,
  selectionReducer,
});
