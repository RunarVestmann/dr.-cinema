import * as constants from '../constants';

export default (state = '', action) => {
  switch (action.type) {
    case constants.GET_BACKDROPS_BY_MOVIE_ID:
      return action.payload.length > 0
        ? action.payload[Math.floor(Math.random() * action.payload.length)]
        : '';
    case constants.RESET_BACKDROPS:
      return '';
    default:
      return state;
  }
};
