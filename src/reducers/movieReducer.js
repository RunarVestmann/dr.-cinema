import * as constants from '../constants';

export default (state = [], action) => {
  switch (action.type) {
    case constants.GET_ALL_MOVIES:
      return action.payload;
    case constants.GET_UPCOMING_MOVIES:
      return action.payload;
    case constants.GET_MOVIES_BY_CINEMA_ID:
      return action.payload;
    case constants.RESET_MOVIES:
      return [];
    default:
      return state;
  }
};
