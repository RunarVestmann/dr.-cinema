import * as constants from '../constants';

export default (state = {}, action) => {
  switch (action.type) {
    case constants.SELECT_CINEMA:
      return { ...state, cinema: action.payload };
    case constants.SELECT_MOVIE:
      return { ...state, movie: action.payload };
    default:
      return state;
  }
};
