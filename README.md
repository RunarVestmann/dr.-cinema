**Icelandic cinema and movie app written in React Native (Expo)**

Home screen

![Home screen](screenshots/0home_screen.png)

Cinemas screen

![Cinemas screen](screenshots/1cinemas_screen.png)

Cinema details screen

![Cinema details screen](screenshots/2cinema_details_screen.png)

Movie details screen

![Movie details screen](screenshots/3movie_details_screen.png)

Upcoming movies screen

![Upcoming movies screen](screenshots/4upcoming_movies_screen.png)

Movie details screen

![Movie details screen](screenshots/5movie_details_screen.png)
